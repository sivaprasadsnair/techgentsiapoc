﻿using Expeed.TechgentsiaPOC.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Expeed.TechgentsiaPOC
{
    public partial class App : Application
    {
        public static double ScreenWidth;
        public static double ScreenHeight;
        public static VideoEngine video;
        public static int WidthX;
        public static int HeightY;
        public static double CallStartTime;
        public static int WidthPixels;
        public static int HeightPixels;
        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new MainPage());           
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
