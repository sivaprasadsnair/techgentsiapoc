﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expeed.TechgentsiaPOC.DependencyServices
{
    public interface IVideoControl
    {
        void EndVideoCall();
        void MuteCall();
        void OnNotesView(bool IsNotes);
        void PauseVideo();
        void ResumeVideo();
        void NetworkState(bool state);
        void NextAppointment(string data, string starttime);

    }
}
