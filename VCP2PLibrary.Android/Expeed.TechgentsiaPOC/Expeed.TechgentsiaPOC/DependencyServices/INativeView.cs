﻿using Expeed.TechgentsiaPOC.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Expeed.TechgentsiaPOC.DependencyServices
{
    public interface INativeView
    {
       void OpenNativeView(ConferenceParameters conferenceParams);
    }
}
