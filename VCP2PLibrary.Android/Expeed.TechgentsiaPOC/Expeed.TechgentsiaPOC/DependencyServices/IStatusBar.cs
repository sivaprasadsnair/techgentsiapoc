﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expeed.TechgentsiaPOC.DependencyServices
{
    public interface IStatusBar
    {
        void HideStatusBar();
        void ShowStatusBar();
    }
}
