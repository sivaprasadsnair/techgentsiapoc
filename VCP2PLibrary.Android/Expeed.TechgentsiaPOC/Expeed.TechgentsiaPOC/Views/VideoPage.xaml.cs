﻿using Expeed.TechgentsiaPOC.DependencyServices;
using Expeed.TechgentsiaPOC.Models;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Expeed.TechgentsiaPOC.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VideoPage : ContentPage
    {
        ConferenceParameters conferenceParams = new ConferenceParameters();       

        private bool isNotes = false;
        public bool IsNotes
        {
            get { return isNotes; }
            set { isNotes = value; }
        }
        public VideoPage(string name,string cid)
        {
            InitializeComponent();
            video.OnCallDisconnect += OnDisconnect;
            video.OnNotesClicked += OnNotesClicked;
            video.OnEnlargeVideo += OnVideoEnlarge;
            video.FetchNextAppointment += FetchNextAppointment;
            //video.OnConnectionEstablished
            video.Duration = 80;// 300;
            App.video = video;
            conferenceParams.Name = name;
            //Guid obj = Guid.NewGuid();
            conferenceParams.RoomId = cid;// obj.ToString();
            SetVideoConferenceParams();
            BindingContext = this;

        }

        private void FetchNextAppointment()
        {
           
            //  string next = "You have an appointment at " + startTime + " today";
            //   video.NextAppointmentData(next, startTime);
         
        }

        private void OnDisconnect()
        {
            Navigation.PopAsync();
            Navigation.PushAsync(new MainPage());
        }

        private void OnNotesClicked()
        {
            IsNotes = true;
        }

        private void OnVideoEnlarge()
        {

        }

        protected override bool OnBackButtonPressed()
        {
            //IsNotes = !IsNotes;
            //if (IsNotes)
            //{
            //}
            //video.OnNotesView(IsNotes);
            
            return true;
        }

        private void SetVideoConferenceParams()
        {
            IceServer server;
            List<IceServer> iceServers = new List<IceServer>();
            server = new IceServer();
            server.Uri = "stun:stun.l.google.com:19302";
            server.Username = server.Password = server.Policy = "";
            iceServers.Add(server);
            server = new IceServer();
            server.Uri = "stun:stun1.l.google.com:19302";
            server.Username = server.Password = server.Policy = "";
            iceServers.Add(server);
            server = new IceServer();
            server.Uri = "stun:stun3.l.google.com:19302";
            server.Username = server.Password = server.Policy = "";
            iceServers.Add(server);
            server = new IceServer();
            server.Uri = "stun:stun4.l.google.com:19302";
            server.Username = server.Password = server.Policy= "";
            iceServers.Add(server);
            server = new IceServer();
            server.Uri = "turn:turn.vconsol.com";
            server.Username = "vconsolturnuser"; server.Password = "vconsolturnpassword"; server.Policy = "NoCheck";
            iceServers.Add(server);
            server = new IceServer();
            server.Uri = "turn:turn.vconsol.com:3478?transport=udp";
            server.Username = "vconsolturnuser"; server.Password = "vconsolturnpassword"; server.Policy = "NoCheck";
            iceServers.Add(server);
            server = new IceServer();
            server.Uri = "turns:turn.vconsol.com:443?transport=tcp"; server.Policy = "NoCheck";
            server.Username = "vconsolturnuser"; server.Password = "vconsolturnpassword";
            iceServers.Add(server);
            conferenceParams.IceServers = iceServers;
            Signalling signalling = new Signalling();
            signalling.BaseUrl = "https://p2p.techgentsia.com/";
            signalling.Path= "/webconf/socket.io";
            conferenceParams.Signalling = signalling;
            //DependencyService.Get<INativeView>().OpenNativeView(conferenceParams);
            video.SetConferenceParameters(conferenceParams);
        }

    }
}