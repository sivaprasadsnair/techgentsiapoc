﻿using Expeed.TechgentsiaPOC.Views;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Expeed.TechgentsiaPOC
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            name.Text = "Anju";
            cid.Text = "123";
        }
        private async void Button_Clicked(object sender, EventArgs e)
        {
            //SetVideoConferenceParams();
            if (await GetPermissions().ConfigureAwait(false))
            {
                Device.BeginInvokeOnMainThread(async () =>
                {

                    Navigation.PushAsync(new VideoPage(name.Text, cid.Text));
                });
            }
        }
        public async Task<bool> GetPermissions()
        {
            bool permissionsGranted = true;

            var permissionsStartList = new List<Permission>()
                                        {
                                            Permission.Camera,
                                            Permission.Microphone
                                        };

            var permissionsNeededList = new List<Permission>();
            try
            {
                foreach (var permission in permissionsStartList)
                {
                    var status = await CrossPermissions.Current.CheckPermissionStatusAsync(permission);
                    if (status != PermissionStatus.Granted)
                    {
                        permissionsNeededList.Add(permission);
                    }
                }
            }
            catch (Exception)
            {
            }


            try
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(permissionsNeededList.ToArray());
                foreach (var permission in permissionsNeededList)
                {
                    var status = PermissionStatus.Unknown;
                    //Best practice to always check that the key exists
                    if (results.ContainsKey(permission))
                        status = results[permission];
                    if (status == PermissionStatus.Granted || status == PermissionStatus.Unknown)
                    {
                        permissionsGranted = true;
                    }
                    else
                    {
                        permissionsGranted = false;
                        break;
                    }
                }
                //var status = await CrossPermissions.Current.RequestPermissionAsync<CameraPermission>();
                //if (status == PermissionStatus.Granted)
                //    permissionsGranted = true;
                //else
                //    permissionsGranted = false;
            }
            catch (Exception)
            {
            }
            return permissionsGranted;
        }
    }
}
