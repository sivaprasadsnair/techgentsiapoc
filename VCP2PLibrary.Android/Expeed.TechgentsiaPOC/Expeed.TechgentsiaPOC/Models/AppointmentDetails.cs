﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expeed.TechgentsiaPOC.Models
{
    public class AppointmentDetails
    {
        public int productId { get; set; }
        public int counsellorId { get; set; }
        public int clientId { get; set; }
        public DateTime AppointmentDate { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
    }
}
