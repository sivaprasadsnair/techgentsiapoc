﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Expeed.TechgentsiaPOC.Models
{
    public class ConferenceParameters
    {
        public string Name { get; set; }
        public string RoomId { get; set; }
        public int Duration { get; set; }
        public bool IsTimeValid { get; set; }
        public int ShowEndTimeWarningBefore { get; set; }
        public List<IceServer> IceServers { get; set; }
        public Signalling Signalling { get; set; }
        public VideoCallToken videoCallToken { get; set; }
    }
    public class IceServer
    {
        public string Uri { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Policy { get; set; }
    }

    public class Signalling
    {
        public string BaseUrl { get; set; }
        public string Path { get; set; }
    }
}
