﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expeed.TechgentsiaPOC.Models
{
    public class VideoCallToken
    {
        public string Token { get; set; }
        public string UniqueID { get; set; }
    }
}
