﻿using Expeed.TechgentsiaPOC.DependencyServices;
using Expeed.TechgentsiaPOC.Models;
using System;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Expeed.TechgentsiaPOC
{
    public class VideoEngine:View 
    {

        private const string IsNotesPropertyName = "IsNotes";

        public static readonly BindableProperty IsNotesProperty =
            BindableProperty.Create(IsNotesPropertyName,
                                    typeof(bool),
                                    typeof(VideoEngine),
                                    false);

        public bool IsShowNotes
        {
            get => (bool)GetValue(IsNotesProperty);
            set => SetValue(IsNotesProperty, value);
        }
        public int Duration
        {
            get => (int)GetValue(durationProperty);
            set => SetValue(durationProperty, value);
        }
        private static readonly BindableProperty durationProperty = BindableProperty.Create(
                                                         propertyName: "Duration",
                                                         returnType: typeof(int),
                                                         declaringType: typeof(VideoEngine),
                                                         defaultValue: 0,
                                                         defaultBindingMode: BindingMode.OneTime,
                                                         propertyChanged: ondurationchange);
        private static void ondurationchange(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (VideoEngine)bindable;
        }

        public int CallWarningTime
        {
            get => (int)GetValue(callWarningTimeProperty);
            set => SetValue(callWarningTimeProperty, value);
        }
        private static readonly BindableProperty callWarningTimeProperty = BindableProperty.Create(
                                                         propertyName: "CallWarningTime",
                                                         returnType: typeof(int),
                                                         declaringType: typeof(VideoEngine),
                                                         defaultValue: 0,
                                                         defaultBindingMode: BindingMode.OneTime,
                                                         propertyChanged: oncallwarningtimechange);
        private static void oncallwarningtimechange(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (VideoEngine)bindable;
        }

        public string UserRole
        {
            get => (string)GetValue(UserRoleProperty);
            set => SetValue(UserRoleProperty, value);
        }

        public static readonly BindableProperty UserRoleProperty =
           BindableProperty.Create("UserRole",
                                   typeof(string),
                                   typeof(VideoEngine),
                                   false);

        public delegate void OnViewTappedDelegate();
        public OnViewTappedDelegate OnViewTapped { get; set; }

        public delegate void OnCallDisconnectDelegate();
        public OnCallDisconnectDelegate OnCallDisconnect {
            get;set;
        }

        public delegate void OnNotesDelegate();
        public OnNotesDelegate OnNotesClicked { get; set; }
        public delegate void OnVideoEnlargedDelegate();
        public OnVideoEnlargedDelegate OnEnlargeVideo { get; set; }

        public delegate void OnConnectionEstablishedDelegate();
        public OnConnectionEstablishedDelegate OnConnectionEstablished { get; set; }

        public delegate void FetchNextAppointmentDelegate();
        public FetchNextAppointmentDelegate FetchNextAppointment { get; set; }

        public delegate void NavigateToHomeDelegate();
        public NavigateToHomeDelegate NavigateToHome { get; set; }

        public VideoEngine()
        {
            DependencyService.Get<IStatusBar>().HideStatusBar();
            DeviceDisplay.KeepScreenOn = true;
            TimeSpan time = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            App.CallStartTime = time.TotalSeconds;
        }
        public void PageDisappearing()
        {
            DependencyService.Get<IStatusBar>().ShowStatusBar();
            DeviceDisplay.KeepScreenOn = false;
        }
        public void OnNotesView(bool IsNotes)
        {
            DependencyService.Get<IVideoControl>()?.OnNotesView(IsNotes);
        }
        public void SetConferenceParameters(ConferenceParameters conference)
        {
            DependencyService.Get<INativeView>()?.OpenNativeView(conference);
        }
        public void SetAppVideoReference(VideoEngine video)
        {
            App.video = video;
        }
        public void EndVideoCall()
        {
            DependencyService.Get<IVideoControl>()?.EndVideoCall();
        }
        public void PauseVideo()
        {
            try
            {
                DependencyService.Get<IVideoControl>()?.PauseVideo();
            }
            catch (Exception ex) { }
        }
        public void ResumeVideo()
        {
            try
            {
                DependencyService.Get<IVideoControl>()?.ResumeVideo();
            }
            catch (Exception ex) { }
        }
        public void NetworkState(bool state)
        {
            DependencyService.Get<IVideoControl>().NetworkState(state);
        }

        public void NextAppointmentData(string data, string starttime)
        {
            DependencyService.Get<IVideoControl>().NextAppointment(data,starttime);
        }
    }
}
