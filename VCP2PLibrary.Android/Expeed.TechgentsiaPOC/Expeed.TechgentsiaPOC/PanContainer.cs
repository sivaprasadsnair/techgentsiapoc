﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Expeed.TechgentsiaPOC
{
    public class PanContainer : ContentView
    {
        double x, y;
        public PanGestureRecognizer PanGestureRecognizer { get; set; }
        public static readonly BindableProperty TouchPointsProperty = BindableProperty.Create("TouchPoints", typeof(int), typeof(PanContainer));
        public int TouchPoints
        {
            get => (int)GetValue(TouchPointsProperty);
            set => SetValue(TouchPointsProperty, value);
        }
        public PanContainer()
        {
            // Set PanGestureRecognizer.TouchPoints to control the
            // number of touch points needed to pan
            PanGestureRecognizer = new PanGestureRecognizer();
            PanGestureRecognizer.TouchPoints = 1;
            PanGestureRecognizer.PanUpdated += OnPanUpdated;
            GestureRecognizers.Add(PanGestureRecognizer);
        }

        public void OnPropertyChange()
        {
            PanGestureRecognizer.TouchPoints = TouchPoints;
        }

        public void OnPanUpdated(object sender, PanUpdatedEventArgs e)
        {
            switch (e.StatusType)
            {
                // Move view
                case GestureStatus.Running:
                    TranslationX = Math.Max(Math.Min(0, TranslationX + e.TotalX), -Math.Abs(Width - App.ScreenWidth));
                    TranslationY = Math.Max(Math.Min(0, TranslationY + e.TotalY), -Math.Abs(Height - App.ScreenHeight));
                    break;
                case GestureStatus.Completed:
                    x = TranslationX;
                    y = TranslationY;
                    break;
            }
        }
    }
}
