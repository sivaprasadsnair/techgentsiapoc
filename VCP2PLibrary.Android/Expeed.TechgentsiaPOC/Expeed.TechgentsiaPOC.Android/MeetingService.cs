﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using Com.Tst.Vcp2plibrary.Interfaces;
using Com.Tst.Vcp2plibrary.Data.Json;
using Com.Tst.Vcp2plibrary.Data.Params;
using Org.Webrtc;
using Com.Tst.Vcp2plibrary;
using Com.Tst.Vcp2plibrary.Util;
using Android.Util;
using AndroidX.ConstraintLayout.Widget;
using Xamarin.Essentials;
using Expeed.TechgentsiaPOC.Models;
using Android.Views.Animations;

namespace Expeed.TechgentsiaPOC.Droid
{
    [Service(Name = "com.expeed.techgentsiapoc.MeetingService")]
    public class MeetingService : Service, IOnMeetingEvents, View.IOnClickListener
    {
        private const string TAG = "VideoLibrary::MeetingService";
        public IBinder Binder { get; private set; }
        private VideoPageViewModel videoPageViewModel;
        TSTVideoView viewPreview = new TSTVideoView(Application.Context);
        TSTVideoView viewRemote;
        ImageButton endCall = new ImageButton(Application.Context);
        ImageButton btnNotes, btnLeft, btnRight, btnTop, btnBottom, btnVideo, btnAudio, fullview;
        ImageButton participantVideo, participantAudio;
        ConstraintLayout constraintLayout;
        VideoEngine video = App.video;
        int screenWidth, screenHeight;
        bool IsThumbView, IsControlsVisible, IsWarningClosed = false;
        LinearLayout controlsLayout, timeLayout, warningLayout, networkMessage;
        RelativeLayout overlay;
        TextView txtremainingTime, lblWarning, lblnextAppointment;
        Button btnCloseWarning;
        bool IsAudio = true, IsVideo = true, IsDisconnected = false, IsUserJoined = false, IsElapsedNextAppWarning = false;
        ViewParams viewParams;
        DateTime MeetingEndTime;
        string NextAppointmentStartTime = "";
        public void MeetingExitedSuccessFully()
        {
            Log.Debug(TAG, "MeetingExitedSuccessFully");
            StopForeground(true);
            ResetOnDisconnect();
        }

        public override IBinder OnBind(Intent intent)
        {
            Log.Debug(TAG, "OnBind");
            Binder = new MeetingServiceBinder(this);

            return Binder;
        }

        public void OnChat(Chat chat)
        {
            Log.Debug(TAG, "OnChat");
            //throw new NotImplementedException();
        }

        public void OnScreenShare(bool started, User user)
        {
            Log.Debug(TAG, "OnScreenShare");
            //throw new NotImplementedException();
        }

        public void OnPauseVideo()
        {
            p2pInterface?.OnPause();
        }

        public void OnResumeVideo()
        {
            p2pInterface?.OnResume(viewParams);
        }

        public void OnUser(bool joined, User user)
        {
            string connected = joined ? " connected" : " disconnected";
            //Log.Debug(TAG, $"OnUser -> {user.Displayname} {connected}");
            if (joined)
            {
                viewPreview.LayoutParameters.Width = 220;
                viewPreview.LayoutParameters.Height = 250;
                IsUserJoined = true;
                Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                {
                    btnAudio.Visibility = btnVideo.Visibility = ViewStates.Visible;
                });
                App.video.OnConnectionEstablished?.Invoke();
            }
            else
            {
                IsUserJoined = false;
                viewPreview.LayoutParameters.Height = ViewGroup.LayoutParams.MatchParent;
                viewPreview.LayoutParameters.Width = ViewGroup.LayoutParams.MatchParent;
                Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                {
                    btnAudio.Visibility = btnVideo.Visibility = ViewStates.Gone;
                });
            }
        }

        public bool InitMeetingRoom(VideoPageViewModel videoPageViewModel)
        {
            Log.Debug(TAG, "InitMeetingRoom");
            this.videoPageViewModel = videoPageViewModel;
            InitVariables();
            GetViews();
            var paramName = VideoPageViewModel.conferenceParams.Name;
            string token = string.Empty;
            //token = TokenGenerator.GenerateJWT(VideoPageViewModel.conferenceParams.RoomId.ToString(), paramName);
            var conferenceParams = new ConferenceParams(Android.App.Application.Context, this, paramName, VideoPageViewModel.conferenceParams.videoCallToken.UniqueID);
            //conferenceParams.SignallingParams = new SignallingParams(token, "https://p2p.techgentsia.com/", "/webconf/socket.io");

            conferenceParams.SignallingParams = new SignallingParams(VideoPageViewModel.conferenceParams.videoCallToken.Token, VideoPageViewModel.conferenceParams.Signalling.BaseUrl, VideoPageViewModel.conferenceParams.Signalling.Path);
            List<PeerConnection.IceServer> iceServers = new List<PeerConnection.IceServer>();
            foreach (IceServer server in VideoPageViewModel.conferenceParams.IceServers)
            {
                if (server.Policy.Trim() == string.Empty)
                {
                    iceServers.Add(new PeerConnection.IceServer(server.Uri, server.Username, server.Password));
                }
                else
                {
                    iceServers.Add(new PeerConnection.IceServer(server.Uri, server.Username, server.Password, PeerConnection.TlsCertPolicy.TlsCertPolicyInsecureNoCheck));
                }
            }
            conferenceParams.IceServers = iceServers;
            viewParams = new ViewParams(
                videoPageViewModel.View.FindViewById<TSTVideoView>(Expeed.TechgentsiaPOC.Droid.Resource.Id.preview),
                new List<TSTVideoView>()
                {
                    videoPageViewModel.View.FindViewById<TSTVideoView>(Expeed.TechgentsiaPOC.Droid.Resource.Id.remvideo_one)
                }
             );

            conferenceParams.ViewParams = viewParams;
            conferenceParams.Name = paramName;

            p2pInterface = new P2PInterface(conferenceParams);
            ShowTimer(App.video.Duration);
            VideoPageViewModel.meetingServiceMutable = this;
            return true;
        }

        public void onResume(ViewParams viewParams)
        {
            p2pInterface?.OnResume(viewParams);
        }

        public void onPause()
        {
            p2pInterface?.OnPause();
        }

        public void endMeeting()
        {
            try
            {
                p2pInterface?.EndMeeting();
            }
            catch (Exception ex)
            {
                ResetOnDisconnect();
            }
        }
        public void EnableDisableAudio(bool IsAudio)
        {
            p2pInterface?.EnableAudio(IsAudio);
        }
        public void EnableDisableVideo(bool IsVideo)
        {
            p2pInterface?.EnableVideo(IsVideo);
        }
        public void OnAudioStatus(MediaStatus status)
        {
            try
            {
                Xamarin.Forms.Device.BeginInvokeOnMainThread(() => {
                    if (status.Muted)
                        participantAudio.Visibility = ViewStates.Visible;
                    else
                        participantAudio.Visibility = ViewStates.Gone;

                });

            }
            catch (Exception ex) { Log.Debug(TAG, "Exception in OnAudioStatus :" + ex.Message); }
        }

        public void OnCustomEvent(CustomDataEvent customDataEvent)
        {
            //throw new NotImplementedException();
        }

        public void OnVideoStatus(MediaStatus status)
        {
            try
            {
                Xamarin.Forms.Device.BeginInvokeOnMainThread(() => {
                    if (status.Muted)
                    {
                        participantVideo.Visibility = ViewStates.Visible;
                        viewRemote.LayoutParameters.Height = ViewGroup.LayoutParams.WrapContent;
                    }
                    else
                    {
                        participantVideo.Visibility = ViewStates.Gone;
                        viewRemote.LayoutParameters.Height = ViewGroup.LayoutParams.MatchParent;
                    }

                });

            }
            catch (Exception ex) { Log.Debug(TAG, "Exception in OnVideoStatus :" + ex.Message); }
        }
        private void DisconnectVideoCall(object sender, EventArgs e)
        {
            if (!IsDisconnected)
                endMeeting();
            else
            {
                ResetOnDisconnect();
            }
            App.video.OnCallDisconnect?.Invoke();
        }

        public void AutoDisconnectVideoCall()
        {
            if (!IsDisconnected)
                endMeeting();
            else
                ResetOnDisconnect();
        }
        private void ResetOnDisconnect()
        {
            SetViewSize("F");
            if (constraintLayout != null)
                constraintLayout.Visibility = ViewStates.Gone;
            IsDisconnected = true;
            IsControlsVisible = true;
            IsThumbView = false;
            videoPageViewModel.nextAppointment = "";
            if (videoPageViewModel != null && videoPageViewModel.View != null)
            {
                ViewGroup vg = (ViewGroup)(videoPageViewModel.View.Parent);
                if (vg != null && vg.FindViewById<View>(videoPageViewModel.View.Id) != null)
                    vg.RemoveView(videoPageViewModel.View);
            }
        }

        public void ShowTimer(int duration)
        {
            TimeSpan d = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            double diff = d.TotalSeconds - App.CallStartTime;
            duration = duration - (int)diff;
            int counter = duration;
            double elapseCounter = 0;
            if (counter > 0)
            {
                //timeStack.IsVisible = true;
                Xamarin.Forms.Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                {
                    counter--;
                    if (counter >= 0)
                    {
                        TimeSpan t = TimeSpan.FromSeconds(counter);
                        string remainingTime = string.Format("Remaining Time : {1:D2}:{2:D2}",
                                t.Hours,
                                t.Minutes,
                                t.Seconds);
                        txtremainingTime.Text = remainingTime;
                        lblWarning.Text = string.Format("{1:D2}:{2:D2} left in scheduled meeting time",
                                t.Hours,
                                t.Minutes,
                                t.Seconds);

                    }
                    else
                    {
                        TimeSpan d = new TimeSpan(MeetingEndTime.Hour, MeetingEndTime.Minute, MeetingEndTime.Second);
                        TimeSpan now = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                        elapseCounter = now.TotalSeconds - d.TotalSeconds;
                        TimeSpan elapsed = TimeSpan.FromSeconds(elapseCounter);

                        txtremainingTime.Text = string.Format("Elapsed Time : {1:D2}:{2:D2}",
                                elapsed.Hours,
                                elapsed.Minutes,
                                elapsed.Seconds);
                        lblWarning.Text = string.Format("{1:D2}:{2:D2} more elapsed from scheduled meeting end time",
                                elapsed.Hours,
                                elapsed.Minutes,
                                elapsed.Seconds);
                    }
                    if (!string.IsNullOrEmpty(videoPageViewModel.nextAppointment))
                    {
                        lblnextAppointment.Text = videoPageViewModel.nextAppointment;
                        lblnextAppointment.Visibility = ViewStates.Visible;
                    }
                    if (counter == 0)
                    {
                        MeetingEndTime = DateTime.Now;
                        IsWarningClosed = false;
                        //DisconnectVideoCall(this, null);
                        //App.videoEngine.OnCallDisconnect?.Invoke();
                    }
                    else if (counter <= App.video.CallWarningTime * 60)
                    {
                        if (!IsWarningClosed && !IsThumbView)
                            warningLayout.Visibility = ViewStates.Visible;
                        if (App.video.UserRole == "Counsellor" && !IsDisconnected && counter <= 300 && string.IsNullOrEmpty(videoPageViewModel.nextAppointment))
                            App.video.FetchNextAppointment?.Invoke();
                    }
                    if (counter < 0 && !string.IsNullOrEmpty(NextAppointmentStartTime))
                    {
                        TimeSpan start = DateTime.Parse(NextAppointmentStartTime).TimeOfDay;
                        DateTime startTime = new DateTime();
                        startTime = DateTime.Now.Date.Add(start);
                        DateTime currentTime = DateTime.Now;
                        if (!IsElapsedNextAppWarning && startTime.AddMinutes(-5) <= currentTime)
                        {
                            IsWarningClosed = false;
                            IsElapsedNextAppWarning = true;
                        }
                    }
                    if (IsDisconnected)
                    {
                        return false;
                    }
                    else
                        return true;
                    //return Convert.ToBoolean(counter);
                });
            }
            else
            {
                //Xamarin.Forms.Application.Current.MainPage = new AppShell();
                App.video.NavigateToHome?.Invoke();
                AutoDisconnectVideoCall();
            }
        }


        private P2PInterface p2pInterface;
        public void GetViews()
        {
            constraintLayout = videoPageViewModel.View.FindViewById<ConstraintLayout>(Resource.Id.videocallview);
            endCall = videoPageViewModel.View.FindViewById<ImageButton>(Resource.Id.endmeeting);

            viewRemote = videoPageViewModel.View.FindViewById<TSTVideoView>(Resource.Id.remvideo_one);
            viewPreview = videoPageViewModel.View.FindViewById<TSTVideoView>(Resource.Id.preview);
            viewPreview.SetOnClickListener(this);
            viewRemote.SetOnClickListener(this);
            viewPreview.ClearImage();
            viewRemote.ClearImage();
            endCall.Clickable = true;
            endCall.Click += DisconnectVideoCall;

            fullview = videoPageViewModel.View.FindViewById<ImageButton>(Resource.Id.fullscreen);
            fullview.Click += Fullview_Click;
            btnNotes = videoPageViewModel.View.FindViewById<ImageButton>(Resource.Id.notes);
            btnNotes.Click += BtnNotes_Click;
            btnNotes.Visibility = video.IsShowNotes ? ViewStates.Visible : ViewStates.Gone;
            btnAudio = videoPageViewModel.View.FindViewById<ImageButton>(Resource.Id.audio);
            btnAudio.Click += BtnAudio_Click;
            btnVideo = videoPageViewModel.View.FindViewById<ImageButton>(Resource.Id.video);
            btnVideo.Click += BtnVideo_Click;
            btnAudio.Visibility = btnVideo.Visibility = ViewStates.Gone;
            controlsLayout = videoPageViewModel.View.FindViewById<LinearLayout>(Resource.Id.relControls);

            overlay = videoPageViewModel.View.FindViewById<RelativeLayout>(Resource.Id.overlay);
            overlay.Alpha = 0.3F;
            overlay.Visibility = ViewStates.Gone;

            btnLeft = videoPageViewModel.View.FindViewById<ImageButton>(Resource.Id.left);
            btnLeft.Click += BtnLeft_Click;
            btnRight = videoPageViewModel.View.FindViewById<ImageButton>(Resource.Id.rightgo);
            btnRight.Click += BtnRight_Click;
            btnTop = videoPageViewModel.View.FindViewById<ImageButton>(Resource.Id.top);
            btnTop.Click += BtnTop_Click;
            btnBottom = videoPageViewModel.View.FindViewById<ImageButton>(Resource.Id.bottomgo);
            btnBottom.Click += BtnBottom_Click;

            txtremainingTime = videoPageViewModel.View.FindViewById<TextView>(Resource.Id.remainingTime);
            timeLayout = videoPageViewModel.View.FindViewById<LinearLayout>(Resource.Id.timeLayout);
            warningLayout = videoPageViewModel.View.FindViewById<LinearLayout>(Resource.Id.lnrWarning);
            lblWarning = videoPageViewModel.View.FindViewById<TextView>(Resource.Id.lblWarning);
            lblnextAppointment = videoPageViewModel.View.FindViewById<TextView>(Resource.Id.lblnxtapnt);
            btnCloseWarning = videoPageViewModel.View.FindViewById<Button>(Resource.Id.btncloseWarning);
            btnCloseWarning.Click += BtnCloseWarning_Click;
            participantAudio = videoPageViewModel.View.FindViewById<ImageButton>(Resource.Id.participantaudio);
            participantAudio.Visibility = ViewStates.Gone;
            participantVideo = videoPageViewModel.View.FindViewById<ImageButton>(Resource.Id.participantvideo);
            participantVideo.Visibility = ViewStates.Gone;
            networkMessage = videoPageViewModel.View.FindViewById<LinearLayout>(Resource.Id.networkerror);
        }
        private void InitVariables()
        {
            IsAudio = true;
            IsVideo = true;
            IsDisconnected = false;
            IsUserJoined = false;
            IsWarningClosed = false;
            IsElapsedNextAppWarning = false;
            IsThumbView = false;
            IsControlsVisible = true;
            videoPageViewModel.nextAppointment = "";
        }
        private void BtnCloseWarning_Click(object sender, EventArgs e)
        {
            IsWarningClosed = true;
            warningLayout.Visibility = ViewStates.Gone;
        }

        private void BtnVideo_Click(object sender, EventArgs e)
        {
            IsVideo = !IsVideo;
            if (IsVideo)
                btnVideo.SetImageResource(Expeed.TechgentsiaPOC.Droid.Resource.Mipmap.video);
            else
                btnVideo.SetImageResource(Expeed.TechgentsiaPOC.Droid.Resource.Mipmap.novideo);
            EnableDisableVideo(IsVideo);
        }

        private void BtnAudio_Click(object sender, EventArgs e)
        {
            IsAudio = !IsAudio;
            if (IsAudio)
                btnAudio.SetImageResource(Expeed.TechgentsiaPOC.Droid.Resource.Mipmap.mute);
            else
                btnAudio.SetImageResource(Expeed.TechgentsiaPOC.Droid.Resource.Mipmap.unmute);
            EnableDisableAudio(IsAudio);
        }

        private void BtnBottom_Click(object sender, EventArgs e)
        {
            constraintLayout.SetY(constraintLayout.GetY() + 50);
        }

        private void BtnTop_Click(object sender, EventArgs e)
        {
            constraintLayout.SetY(constraintLayout.GetY() - 50);
        }

        private void BtnRight_Click(object sender, EventArgs e)
        {
            constraintLayout.SetX(constraintLayout.GetX() + 30);
        }

        private void BtnLeft_Click(object sender, EventArgs e)
        {
            constraintLayout.SetX(constraintLayout.GetX() - 50);
        }

        private void BtnNotes_Click(object sender, EventArgs e)
        {
            SetViewSize("S");
            App.video.OnNotesClicked?.Invoke();
        }

        private void Fullview_Click(object sender, EventArgs e)
        {
            SetViewSize("F");
            if (participantVideo.Visibility == ViewStates.Visible)
                viewRemote.LayoutParameters.Height = ViewGroup.LayoutParams.WrapContent;
            App.video.OnEnlargeVideo?.Invoke();
        }

        public void SetViewSize(string size)
        {
            //App.WidthPixels = Resources.DisplayMetrics.WidthPixels;
            if (size == "S")
            {
                constraintLayout.LayoutParameters.Width = 260;
                constraintLayout.LayoutParameters.Height = 410;
                constraintLayout.RequestLayout();
                IsThumbView = true;
                IsControlsVisible = false;
                controlsLayout.Visibility = ViewStates.Gone;
                timeLayout.Visibility = ViewStates.Gone;
                viewPreview.Visibility = IsUserJoined ? ViewStates.Gone : ViewStates.Visible;
                warningLayout.Visibility = ViewStates.Gone;
                constraintLayout.SetY(150);
                constraintLayout.SetX((float)Resources.DisplayMetrics.WidthPixels - (constraintLayout.LayoutParameters.Width + 14));
            }
            else
            {
                constraintLayout.LayoutParameters.Width = ViewGroup.LayoutParams.MatchParent;
                constraintLayout.LayoutParameters.Height = ViewGroup.LayoutParams.MatchParent;
                viewRemote.LayoutParameters.Width = ViewGroup.LayoutParams.MatchParent;
                viewRemote.LayoutParameters.Height = ViewGroup.LayoutParams.MatchParent;
                overlay.Visibility = ViewStates.Gone;
                timeLayout.Visibility = ViewStates.Visible;
                viewPreview.Visibility = ViewStates.Visible;
                constraintLayout.SetY(0);
                constraintLayout.SetX(0);
                constraintLayout.RequestLayout();
                IsThumbView = false;
                //IsControlsVisible = true;

            }
        }

        public void OnClick(View v)
        {
            if (!IsThumbView)
            {
                controlsLayout.Visibility = IsControlsVisible ? ViewStates.Gone : ViewStates.Visible;
                IsControlsVisible = !IsControlsVisible;
                overlay.Visibility = ViewStates.Gone;
            }
            else
            {
                overlay.Visibility = overlay.Visibility == ViewStates.Visible ? ViewStates.Gone : ViewStates.Visible;
            }
        }
        public void NetworkState(bool state)
        {
            if (networkMessage != null)
            {
                if (state)
                    networkMessage.Visibility = ViewStates.Gone;
                else
                    networkMessage.Visibility = ViewStates.Visible;
            }
        }

        public void NextAppointment(string data, string starttime)
        {
            videoPageViewModel.nextAppointment = data;
            NextAppointmentStartTime = starttime;
            IsWarningClosed = false;
        }

    }

    public class MeetingServiceBinder : Binder
    {
        public MeetingServiceBinder(MeetingService service)
        {
            this.Service = service;
        }

        public MeetingService Service { get; private set; }
    }


    public class MeetingServiceConnection : Java.Lang.Object, Android.Content.IServiceConnection
    {
        static readonly string TAG = "VideoLibrary::MeetingServiceConnection";
        public bool IsConnected { get; private set; }
        public MeetingServiceBinder Binder { get; private set; }
        private readonly VideoPageViewModel videoPageViewModel;
        public MeetingServiceConnection(VideoPageViewModel videoPageViewModel)
        {
            IsConnected = false;
            Binder = null;
            this.videoPageViewModel = videoPageViewModel;
        }
        public void OnServiceConnected(ComponentName name, IBinder service)
        {
            Log.Debug(TAG, "OnServiceConnected");
            videoPageViewModel.serviceBound = true;
            Binder = service as MeetingServiceBinder;
            IsConnected = this.Binder != null;
            if (IsConnected)
            {

                Binder.Service.InitMeetingRoom(videoPageViewModel);
            }
            else
            {

            }
        }

        public void OnServiceDisconnected(ComponentName name)
        {
            Log.Debug(TAG, "OnServiceDisconnected");
        }

    }
}