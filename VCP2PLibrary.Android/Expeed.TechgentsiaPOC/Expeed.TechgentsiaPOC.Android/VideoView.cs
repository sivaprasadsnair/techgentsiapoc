﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidX.ConstraintLayout.Widget;
using Com.Tst.Vcp2plibrary.Util;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using static Android.Views.ViewGroup;
using PermissionStatus = Plugin.Permissions.Abstractions.PermissionStatus;

namespace Expeed.TechgentsiaPOC.Droid
{
    public class VideoView : View
    {
        Activity activity;
        VideoPageViewModel videoPageViewModel = new VideoPageViewModel();
        IServiceConnection serviceConnection;
        View view ;
        public VideoView(Context context) : base(context)
        {
            //ImageButton button = FindViewById<ImageButton>(Resource.Id.participantaudio);
            //int layoutview = FindViewById<View>(Resource.Layout.SingleParticipantLayout).Id;
            activity = context as Activity;
            var root =(ViewGroup) activity.Window.DecorView.RootView;
            LayoutInflater inflater = (LayoutInflater)context.GetSystemService(Context.LayoutInflaterService);            
            view = inflater.Inflate(Expeed.TechgentsiaPOC.Droid.Resource.Layout.SingleParticipantLayout, root,false);            
            activity.AddContentView(view, new LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent));
            InitService();
        }
        void InitService()
        {
            if (!videoPageViewModel.serviceStarted)
            {
                //TSTLogger.print(TAG, "starting service ...")
                videoPageViewModel.serviceStarted = true;
                startMeetingService();
            }
        }

        private void startMeetingService()
        {
            videoPageViewModel.View = view;
            if (null == serviceConnection)
            {
                serviceConnection = new MeetingServiceConnection(videoPageViewModel);
            }

            Intent serviceToStart = new Intent(this.Context, typeof(MeetingService));
            activity = this.Context as Activity;
            activity.BindService(serviceToStart, this.serviceConnection, Bind.AutoCreate);
        }      
       
    }
}