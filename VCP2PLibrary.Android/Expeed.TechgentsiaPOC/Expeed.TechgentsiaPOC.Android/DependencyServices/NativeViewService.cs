﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Expeed.TechgentsiaPOC.DependencyServices;
using Expeed.TechgentsiaPOC.Droid.DependencyServices;
using Expeed.TechgentsiaPOC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[assembly: Xamarin.Forms.Dependency(typeof(NativeViewService))]
namespace Expeed.TechgentsiaPOC.Droid.DependencyServices
{
    public class NativeViewService : INativeView
    {
        public void OpenNativeView(ConferenceParameters conferenceParams)
        {
            VideoPageViewModel.conferenceParams = conferenceParams;
        }
    }
}