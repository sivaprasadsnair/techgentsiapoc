﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Expeed.TechgentsiaPOC.DependencyServices;
using Expeed.TechgentsiaPOC.Droid.DependencyServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[assembly: Xamarin.Forms.Dependency(typeof(VideoControlService))]
namespace Expeed.TechgentsiaPOC.Droid.DependencyServices
{
    public class VideoControlService : IVideoControl
    {
        MeetingService meeting = VideoPageViewModel.meetingServiceMutable;
        public void EndVideoCall()
        {
            meeting?.AutoDisconnectVideoCall();
        }

        public void MuteCall()
        {
            throw new NotImplementedException();
        }

        public void OnNotesView(bool IsNotes)
        {
            if (IsNotes)
                meeting?.SetViewSize("S");
            else
                meeting?.SetViewSize("F");
        }
        public void PauseVideo()
        {
            meeting?.OnPauseVideo();
        }

        public void ResumeVideo()
        {
            meeting?.OnResumeVideo();
        }
        public void NetworkState(bool state)
        {
            meeting?.NetworkState(state);
        }

        public void NextAppointment(string data, string starttime)
        {
            meeting?.NextAppointment(data,starttime);
        }
    }
}