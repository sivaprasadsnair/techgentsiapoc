﻿using AndroidX.Lifecycle;
using Expeed.TechgentsiaPOC.Models;

namespace Expeed.TechgentsiaPOC.Droid
{
    public class VideoPageViewModel : ViewModel
    {
        public VideoPageViewModel()
        {

        }

        public bool serviceStarted = false;

        public static MeetingService meetingServiceMutable;

        public bool serviceBound;

        public Android.Views.View View;
        public static ConferenceParameters conferenceParams;
        public string nextAppointment = "";

    }
}