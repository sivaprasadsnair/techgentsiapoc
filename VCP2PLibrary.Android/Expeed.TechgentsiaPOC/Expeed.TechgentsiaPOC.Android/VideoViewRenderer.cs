﻿using Expeed.TechgentsiaPOC;
using Expeed.TechgentsiaPOC.Droid;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(VideoEngine), typeof(VideoViewRenderer))]
namespace Expeed.TechgentsiaPOC.Droid
{
    [Obsolete]
    public class VideoViewRenderer:ViewRenderer<VideoEngine, VideoView>
    {
        private VideoView videoView;
        protected override void OnElementChanged(ElementChangedEventArgs<VideoEngine> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                if (Control == null)
                {
                    videoView = new VideoView(Context);
                    videoView.Clickable = true;
                    SetNativeControl(videoView);
                }
            }
        }
    }
}